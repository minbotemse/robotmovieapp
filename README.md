# LISEZ-MOI #

Un peu d'aide pour comprendre cette app et ce dépôt.

### L'IA du robot ###

* L'IA du robot sera gérée par un téléphone Android posé sur le robot, communicant avec les cartes Arduino via Bluetooth. L'application MinbotManager, qui gère cette IA, tournera donc sur ce téléphone.
* Ce dépôt contient les sources de MinbotManager, à utiliser avec Android Studio. Le système de contrôle de versions Git est utilisé pour communiquer entre les PC des développeurs et le dépôt central hébergé par Bitbucket. Git est intégré dans Android Studio, donc ça devrait être facile à utiliser.

### MinBotManager ###
* L'application Android permet de se connecter au robot en Bluetooth, puis de le contrôler manuellement ou en mode automatique.
* En mode manuel, le porteur de l'appareil Android contrôle le robot directement.
* En mode automatique, l'application gère la stratégie et le contrôle du robot de manière autonome.

### Informations ###
* Ce dépôt est hébergé à travers mon compte Github. Libre à vous de proposer des modifications, ou même de développer votre propre branche de l'application à partir des sources actuelles ("Fork" qui permettra d'avoir un nouveau dépôt sur votre compte à vous).
* Ce lisez-moi a été rédigé par Victor.