package com.emse.minbot.minbotmanager.ia;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.emse.minbot.minbotmanager.ia.jeu.Afterwork2015;
import com.emse.minbot.minbotmanager.ia.robot.Robot;
import com.emse.minbot.minbotmanager.ia.utils.MoodListener;
import com.emse.minbot.minbotmanager.network.BluetoothManager;
import com.emse.minbot.minbotmanager.utilities.RobotVoice;

import java.util.ArrayList;
import java.util.Scanner;

public class Jarvis extends Service {

    // Service stuff
    private static Jarvis jarvis;
    private final IALocalBinder mBinder = new IALocalBinder();
    private RobotVoice voice;

    // IA stuff
    private MoodListener moodListener;
    private Robot robot;
    private Thread IAThread;
    private boolean IAgoing = true;

//    private PlateauContexte context;
//    private Strategie strategy;

//    private Action currentAction = null;
//    private CheckPoint currentCheckpoint = null;
//    private String instructionStarted;
//    private String instructionFinished;

    public Jarvis() {
    }

    public void setVoice (RobotVoice voice) {
        this.voice = voice;
    }

    //public void start(final CoupeRobotique2015.Couleur couleur) {
    public void start(final Afterwork2015.IAType type) {
        robot = new Robot(type);
        Protocol.establishProtocol();

        IAThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while(IAgoing) {
                    sendInstructions(robot.determineWhatToDo());
                    moodListener.moodChanged(robot.getMood());

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        IAThread.start();

        /*
        new Thread(new Runnable() {
            @Override
            public void run() {
                prepareIA(type);
                BluetoothManager.sendData(new Instruction(Instruction.Type.MODE_AUTOMATIQUE).generateCommand());
            }
        }).start();*/
    }

/* COUPE DE ROBOTIQUE

    private void prepareIA(Afterwork2015.IAType iaType) {
        context = CoupeRobotique2015.genererPlateauContexte(couleur);
        strategy = new Strategie(context);

        strategy.genererActions();
        Collections.shuffle(strategy.getActionsToDo());
    }

    public void nextStep () {

        currentAction = strategy.prochaineActionAFaire(context);
        if(currentAction != null)
            sendInstructions(currentAction.getInstructions());
        else if (!strategy.getActionsToDo().isEmpty()){
            moodListener.moodChanged(Mood.SLEEPING);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(3000);
                        nextStep();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }
//*/

    public void messageFromRobot (String message) {

        if(message.startsWith("OK")) {
            // Nothing to do
        }
        else if (message.startsWith("D")) {
            Scanner scanner = new Scanner(message);
            float angle = scanner.nextFloat();
            robot.somethingDetected(angle);
        }

        /*
        if(message.startsWith("S")) {
            // On recupère l'instruction dans le message de la forme S:instruction
            instructionStarted = message.substring(2);
            interpretMood(instructionStarted);
        }
        else if (message.startsWith("E")) {
            // Dans le cas où une instruction de checkpoint vient de se terminer
            if(message.contains("C")) {
                Scanner scanner = new Scanner(message);
                float x, y;
                x = scanner.nextFloat();
                y = scanner.nextFloat();

                currentCheckpoint = context.getGrid().getClosest(new Vector2D(x, y));
                context.getRobot().setCoordonnees(currentCheckpoint.getPos());
            }
        }
        else {
            // Si tout va bien
            if(message.contains("OK")) {
                if(currentAction != null)
                    strategy.actionEffectuee(true);
                nextStep();
            }
            // Si le robot detecte quelque chose devant, on va interdire le checkpoint qu'il prenait
            else if(message.contains("PC")) {
                moodListener.moodChanged(Mood.SURPRISED);
                voice.speakWords("EXTERMINATE! You won't get me!");
                // Comme ça on reste surpris pendant un petit moment
                eviterProchaineInstructionMood = true;

                // On récupère le checkpoint de la dernière instruction
                Scanner scanner = new Scanner(instructionStarted);
                float x, y;
                x = scanner.nextFloat();
                y = scanner.nextFloat();

                CheckPoint nextCheckPoint = context.getGrid().getClosest(new Vector2D(x, y));
                if(currentCheckpoint != null)
                    currentCheckpoint.forbid(nextCheckPoint, 5000);

                strategy.actionEffectuee(false);
                context.getRobot().setCoordonnees(currentCheckpoint.getPos());
                nextStep();
            }
            // Si le robot rencontre un problème, on annule l'action en cours
            else if(message.contains("PB") || message.contains("Z")) {
                moodListener.moodChanged(Mood.NO_RESPONSE);
                voice.speakWords("What the fuck is happening ?! HELP!");
                // Comme ça on reste surpris pendant un petit moment
                eviterProchaineInstructionMood = true;

                strategy.actionEffectuee(false);
                context.getRobot().setCoordonnees(currentCheckpoint.getPos());
                nextStep();
            }

            // Si le robot a pris un objet, mais que le détecteur n'a rien détecté
            // on désactive l'action
            else if (message.contains("NO")) {
                voice.speakWords("Damn it! Where is it ?! I can't see a thing!");
                strategy.disableAction(currentAction);
                nextStep();
            }
        }
        */
    }

    public static final int MAX_PART_MESSAGE_SIZE = 50;

    public void sendInstructions(final ArrayList<Instruction> instructions) {
        //voice.speakWords("All right, next target!");
        new Thread(new Runnable() {
            @Override
            public void run() {

                /*
                for(Instruction instruction : instructions) {
                    try {
                        BluetoothManager.sendData(instruction.generateCommand());
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }*/
                String message = "";

                for(Instruction instruction : instructions)
                message += instruction.generateCommand();

                BluetoothManager.sendData(message);

                /*
                for(int i = 0; i < totalMessage.length(); i++) {
                    partMessage += totalMessage.charAt(i);
                    messageSize++;

                    if(messageSize >= MAX_PART_MESSAGE_SIZE || i == totalMessage.length()-1) {
                        try {
                            BluetoothManager.sendData(partMessage);
                            partMessage = "";
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }*/


            }
        }).start();
    }

//    private boolean eviterProchaineInstructionMood = false;

    /*
    private void interpretMood (String message) {

        if(eviterProchaineInstructionMood) {
            eviterProchaineInstructionMood = false;
            return;
        }

        Mood mood = null;
        switch (message.charAt(0)) {
            case 'P':
            case 'L':
            case 'Y':
            case 'G':
                voice.speakWords("I hate hard labour...");
                mood = Mood.ANXIOUS;
                break;
            default:
                mood = Mood.OK;
                break;
        }
        moodListener.moodChanged(mood);

    }
//*/
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class IALocalBinder extends Binder {
        public Jarvis getJarvis () {
            return Jarvis.this;
        }
    }

    public void setMoodListener(MoodListener moodListener) {
        this.moodListener = moodListener;
    }

    public void stopIA () {
        IAgoing = false;
    }
}
