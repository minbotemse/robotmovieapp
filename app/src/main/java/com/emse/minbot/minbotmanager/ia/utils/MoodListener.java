package com.emse.minbot.minbotmanager.ia.utils;

/**
 * Created by vincent on 12/05/15.
 */
public interface MoodListener {

    public enum Mood {
        ANXIOUS, NO_RESPONSE, SLEEPING, OK, SURPRISED
    }
    public void moodChanged (Mood mood);
}
