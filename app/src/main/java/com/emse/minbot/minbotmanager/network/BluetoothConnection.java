package com.emse.minbot.minbotmanager.network;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

/**
 * Created by vincent on 01/05/15.
 */
public class BluetoothConnection {

    private static final String TAG = "Minbot";
    private BluetoothAdapter mBluetoothAdapter = null;
    private BluetoothSocket btSocket = null;
    private OutputStream outStream = null;
    private static String address = null;
    private static final UUID MY_UUID = UUID
            .fromString("00001101-0000-1000-8000-00805F9B34FB");
    private InputStream inStream = null;
    Handler handler = new Handler();
    byte delimiter = 10;
    boolean stopWorker = false;
    int readBufferPosition = 0;
    byte[] readBuffer = new byte[4096];

    private boolean connected = false;

    private BluetoothStateReceiver stateReceiver = null;
    private BluetoothDataReceiver dataReceiver = null;

    /**
     * Initialise les différents composants,
     * et repère le composant bluetooth auquel s'adresser
     */
    public String initialize () {
        String result = null;

        result = CheckBt();

        if(result == null) {
            address = null;
            for (BluetoothDevice device : mBluetoothAdapter.getBondedDevices()) {
                if (device.getName().equals("MINBOT")) {
                    address = device.getAddress();
                    break;
                }
            }

            if(address != null) {
                if(stateReceiver != null)
                    stateReceiver.initialized();
                return  null;
            }
            else
                return "Aucun dispositif bluetooth MINBOT trouvé! ";
        }
        else
            return result;
    }

    /**
     * Se connecte au dispositif bluetooth
     */
    public void connect() {

        if(btSocket != null)
            close();

        Log.d(TAG, address);
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        Log.d(TAG, "Connecting to ... " + device);
        stateReceiver.noticeProgress("Connexion à "+device+" en cours...", 50);
        mBluetoothAdapter.cancelDiscovery();
        try {
            btSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
            Log.d(TAG, "Connection made.");
            btSocket.connect();
            connected = true;
        } catch (IOException e) {
            Log.d("MINBOT", e.getMessage(), e);
            try {
                btSocket.close();
            } catch (IOException e2) {
                Log.d(TAG, "Unable to end the connection", e);
            }
            Log.d(TAG, "Socket creation failed");
            if(stateReceiver != null)
                stateReceiver.connectionLost("Impossible de créer la socket");
            connected = false;
        }

        if (connected)
            beginListenForData();
    }

    public void writeData(String data) {
        Log.d(TAG, "Sending stuff : "+data);

        /*
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    dataReceiver.receiveData("WAITING");
                    Thread.sleep(3000);
                    dataReceiver.receiveData("GOING");
                    Thread.sleep(3000);
                    dataReceiver.receiveData("COLLISION");
                    Thread.sleep(3000);
                    dataReceiver.receiveData("GOING BACK");
                    Thread.sleep(3000);
                    dataReceiver.receiveData("GOING");
                    Thread.sleep(3000);
                    stateReceiver.connectionLost("lol");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();*/

        if(!connected)
            return;

        if(btSocket != null) {
            try {
                outStream = btSocket.getOutputStream();
            } catch (Exception e) {
                Log.d(TAG, "Bug BEFORE Sending stuff", e);
                if(stateReceiver != null)
                    stateReceiver.connectionLost("Bug BEFORE Sending stuff");
                connected = false;
            }

            String message = data;
            byte[] msgBuffer = message.getBytes();

            try {
                outStream.write(msgBuffer);
            } catch (Exception e) {
                Log.d(TAG, "Bug while sending stuff", e);
                if(stateReceiver != null)
                    stateReceiver.connectionLost("Bug while sending stuff");
                connected = false;
            }
        }
        else {
            if(dataReceiver != null)
                dataReceiver.receiveData("Data sent : "+data);
        }
    }

    public void close () {
        if(btSocket != null)
            try {
                btSocket.close();
                btSocket = null;
            } catch (Exception e) {
                Log.d(TAG, "Closing socket", e);
                //if(stateReceiver != null)
                //    stateReceiver.connectionLost("Closing socket.");
            }
    }

    public void beginListenForData()   {
        try {
            inStream = btSocket.getInputStream();
        } catch (Exception e) {
            Log.d("MINBOT", e.getMessage(), e);
        }

        Thread workerThread = new Thread(new Runnable()
        {
            public void run()
            {
                while(!Thread.currentThread().isInterrupted() && !stopWorker)
                {
                    try
                    {
                        int bytesAvailable = inStream.available();
                        if(bytesAvailable > 0)
                        {
                            byte[] packetBytes = new byte[bytesAvailable];
                            inStream.read(packetBytes);
                            for(int i=0;i<bytesAvailable;i++)
                            {
                                byte b = packetBytes[i];
                                if(b == delimiter)
                                {
                                    byte[] encodedBytes = new byte[readBufferPosition];
                                    System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                    final String data = new String(encodedBytes, "US-ASCII");
                                    readBufferPosition = 0;
                                    handler.post(new Runnable()
                                    {
                                        public void run()
                                        {
                                            if(dataReceiver != null)
                                                dataReceiver.receiveData(data);
                                        }
                                    });
                                }
                                else
                                {
                                    readBuffer[readBufferPosition++] = b;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        stopWorker = true;
                        Log.d("MINBOT", ex.getMessage(), ex);
                        if(stateReceiver != null) {
                            stateReceiver.connectionLost(ex.getMessage());
                        }
                    }
                }
                connected = false;
            }
        });

        workerThread.start();
        if(stateReceiver != null)
            stateReceiver.connectionEstablished();
    }

    private String CheckBt() {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (mBluetoothAdapter == null) {
            return "Ce portable n'a pas de Bluetooth!!";
        }

        if (!mBluetoothAdapter.isEnabled()) {
            return "Le bluetooth n'est pas activé!";
        }

        // SI TVB
        return null;
    }

    public void connectDataReceiver (BluetoothDataReceiver receiver) {
        this.dataReceiver = receiver;
    }

    public void connectStateReceiver (BluetoothStateReceiver receiver) {
        this.stateReceiver = receiver;
    }

    public boolean isConnected () {
        return btSocket != null && btSocket.isConnected();
    }
}
