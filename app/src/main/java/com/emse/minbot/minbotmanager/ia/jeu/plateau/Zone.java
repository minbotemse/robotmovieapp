package com.emse.minbot.minbotmanager.ia.jeu.plateau;

import com.emse.minbot.minbotmanager.ia.maths.Rectangle;
import com.emse.minbot.minbotmanager.ia.maths.Vector2D;

import java.util.ArrayList;

/**
 * Created by victor on 07/05/15.
 */
public class Zone {

    // Une zone est composee de un ou plusieurs rectangles
    // !! Un rectangle doit etre compose de coordonnees definies dans cet ordre: !!
    // 0--------------------1
    // |                    |
    // |                    |
    // 4--------------------3
    ArrayList<Rectangle> rectangles;

    public Zone() {
        this.rectangles = new ArrayList<>();
    }

    public Zone(Rectangle... rectangles) {
        this.rectangles = new ArrayList<>();
        addRectangles(rectangles);
    }

    public void addRectangles(Rectangle... rectangles) {
        for(Rectangle rectangle : rectangles)
            this.rectangles.add(rectangle);
    }

    public Vector2D getNearestPoint(Vector2D p) {
        Vector2D pTempo, pResultat = null;
        float minDist = Float.MAX_VALUE, dist2;

        for(Rectangle rectangle : rectangles) {
            pTempo = rectangle.closestPoint(p);
            dist2 = pTempo.dist2(p);
            if(dist2 < minDist) {
                pResultat = pTempo;
                minDist = dist2;
            }
        }

        return pResultat;
    }

    public String toString () {
        return "\033[0;1m" + "\u001B[31m" + this.getClass().getSimpleName() + "\033[0;0m ";
    }
}
