package com.emse.minbot.minbotmanager;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.emse.minbot.minbotmanager.network.BluetoothManager;
import com.emse.minbot.minbotmanager.network.BluetoothStateReceiver;

import at.markushi.ui.CircleButton;

/**
 * Created by Vincent on 08/04/2015.
 */
public class BluetoothActivity extends Activity implements BluetoothStateReceiver {

    CircleButton configbutton = null;
    private boolean mBound = false;
    private BluetoothManager bluetoothManager;
    private ProgressBar mProgressBar;
    private TextView mSituationTextView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bluetooth_configuration);

        configbutton = (CircleButton)findViewById(R.id.configButton);
        configbutton.setOnClickListener(configListener);

        mProgressBar = (ProgressBar) findViewById(R.id.connectionProgressBar);
        mSituationTextView = (TextView) findViewById(R.id.infoText);
        
        if(BluetoothManager.getBluetoothManager() != null)
            BluetoothManager.getBluetoothManager().endConnection();
    };

    private View.OnClickListener configListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            noticeProgress("Initialisation en cours...", 10);
            if (!mBound)
                bindBluetoothManager();
            else
                initializeConnection();
        }
    };

    public void initializeConnection () {
        String result;

        noticeProgress("Initialisation en cours...", 10);

        if(BluetoothManager.isConnected())
            bluetoothManager.endConnection();

        result = bluetoothManager.initializeConnection();
        /*
        if(result == null)
            noticeProgress("Bluetooth initialisé!", 40);
        else
            noticeProgress("Problème : \n"+result, 0);*/

        // FOR TESTING PURPOSE!
        //connectionEstablished();
    }

    @Override
    public void initialized() {
        noticeProgress("Bluetooth préparé! ", 30);
        bluetoothManager.startConnection();
    }

    public void connectionEstablished () {
        noticeProgress("Connexion établie!", 100);

        BluetoothManager.registerStateListener(null);
        BluetoothManager.registerDataListener(null);
        Intent intent = new Intent(BluetoothActivity.this, MenuActivity.class);
        startActivity(intent);
    }

    @Override
    public void connectionLost(String reason) {
        noticeProgress("Connexion échouée : "+reason, 0);
    }

    private void bindBluetoothManager () {
        if(!mBound) {
            Intent intent = new Intent(this, BluetoothManager.class);
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        }
    }

    public void noticeProgress(final String text, final int percentage) {
        Log.d("MINBOT", percentage + "% : "+text);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mSituationTextView.setText(text);
                mProgressBar.setProgress(percentage);
            }
        });
    }

    private void register () {
        Log.i("Minbot", "REGISTERED!");
        BluetoothManager.registerStateListener(this);
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            BluetoothManager.BluetoothLocalBinder binder = (BluetoothManager.BluetoothLocalBinder) service;
            bluetoothManager = binder.getBluetoothManager();
            mBound = true;

            register();

            initializeConnection();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
            Log.d("MINBOT", "SERVICE DISCONNECTED!");
        }
    };

    @Override
    protected void onStop(){
        super.onStop();
        if(mBound) {
            //bluetoothManager.endConnection();
            unbindService(mConnection);
            mBound = false;
        }
    }
}

