package com.emse.minbot.minbotmanager.ia.jeu.plateau;

import com.emse.minbot.minbotmanager.ia.jeu.Action;
import com.emse.minbot.minbotmanager.ia.jeu.ActionFermerClapet;
import com.emse.minbot.minbotmanager.ia.jeu.Objectif;
import com.emse.minbot.minbotmanager.ia.maths.Vector2D;

import java.util.ArrayList;

/**
 * Created by victor on 07/05/15.
 */
public class Clapet extends ElementObjectif {

    public Clapet(Vector2D coordonnees) {
        super(coordonnees);
    }

    public Clapet clone() { return new Clapet(coordonnees); }

    public ArrayList<Action> createActions() {
        ArrayList<Action> actions = new ArrayList<>();
        Objectif o = new Objectif(2, this);

        actions.add(new ActionFermerClapet(o, this));

        return actions;
    }
}
