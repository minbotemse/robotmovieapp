package com.emse.minbot.minbotmanager.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.widget.Toast;

import java.util.Locale;

/**
 * Created by vincent on 12/05/15.
 */
public class RobotVoice implements TextToSpeech.OnInitListener {

    private int MY_DATA_CHECK_CODE = 0;
    private TextToSpeech myTTS = null;
    private Context context;

    public RobotVoice (Context context) {
        this.context = context;
    }

    public void create (Activity activity) {
        Intent checkTTSIntent = new Intent();
        checkTTSIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        activity.startActivityForResult(checkTTSIntent, MY_DATA_CHECK_CODE);
    }

    public void speakWords (String speech) {
        if(myTTS != null)
            myTTS.speak(speech, TextToSpeech.QUEUE_ADD, null);
    }

    public void onInit(int initStatus) {
        if (initStatus == TextToSpeech.SUCCESS) {
            if(myTTS.isLanguageAvailable(Locale.UK) == TextToSpeech.LANG_AVAILABLE)
                myTTS.setLanguage(Locale.UK);
            else if(myTTS.isLanguageAvailable(Locale.US) == TextToSpeech.LANG_AVAILABLE)
                myTTS.setLanguage(Locale.US);
            //Toast.makeText(context, "TTS initialized!", Toast.LENGTH_LONG).show();

            personaliseVoice();
        }

        else if (initStatus == TextToSpeech.ERROR) {
            Toast.makeText(context, "Sorry! Text To Speech failed...", Toast.LENGTH_LONG).show();
        }
    }

    private void personaliseVoice () {
        myTTS.setPitch(0.1f);
        myTTS.setSpeechRate(1.0f);
    }

    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
        if (requestCode == MY_DATA_CHECK_CODE) {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                myTTS = new TextToSpeech(activity, this);
            }
            else {
                Intent installTTSIntent = new Intent();
                installTTSIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                activity.startActivity(installTTSIntent);
            }
        }
    }

    public void stop() {
        myTTS.shutdown();
    }
}
