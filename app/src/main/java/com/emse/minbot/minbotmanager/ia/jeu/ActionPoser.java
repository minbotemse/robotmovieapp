package com.emse.minbot.minbotmanager.ia.jeu;

import com.emse.minbot.minbotmanager.ia.Instruction;
import com.emse.minbot.minbotmanager.ia.algo.AStar;
import com.emse.minbot.minbotmanager.ia.jeu.plateau.ElementAttrapable;
import com.emse.minbot.minbotmanager.ia.jeu.plateau.Gobelet;
import com.emse.minbot.minbotmanager.ia.jeu.plateau.Pince;
import com.emse.minbot.minbotmanager.ia.jeu.plateau.Zone;
import com.emse.minbot.minbotmanager.ia.maths.Vector2D;

import java.util.ArrayList;

/**
 * Created by victor on 07/05/15.
 */
public class ActionPoser extends Action {
    protected final static double dureePrevue = 0;
    PlateauContexte contexte;
    Vector2D coordonnees;
    ElementAttrapable element;
    Zone zone;
    Pince pince;
    private ArrayList<CheckPoint> robotPath;

    public ActionPoser(Objectif objectif, ElementAttrapable element, Zone zone) {
        super(objectif);
        this.element = element;
        this.zone = zone;
    }

    private Pince getPinceCorrespondante(Pince[] pinces) {
        for (Pince p: pinces) {
            if (p.getObjet() == element)
                return p;
        }
        return null;
    }

    // TODO: poser à des endroits distincts ?
    @Override
    public void processContext(PlateauContexte contexte) {
        this.contexte = contexte;
        coordonnees = zone.getNearestPoint(contexte.getRobot().getCoordonnees());
        this.contexte = contexte;
        pince = getPinceCorrespondante(contexte.getRobot().getPinces());

        if(pince == null)
            return;

        // On va à présent générer les différents instructions pour cette action
        // On commence par récupérer quelques informations sur le contexte
        CheckPoint checkPointRobot = contexte.getGrid().getClosest(contexte.getRobot().getCoordonnees());
        CheckPoint checkPointZone = contexte.getGrid().getClosest(coordonnees);

        // On génère ensuite le chemin pour aller du robot à la zone
        robotPath = AStar.FindPath(checkPointRobot, checkPointZone);

        // Si l'IA ne trouve pas de chemin, on passe
        if(robotPath == null)
            return;

        instructions.clear();

        instructions.add(new Instruction(Instruction.Type.DETECTION_AVANT, 1));

        // On ajoute tous les checkpoints intermédiaires
        for(int i = 0; i < robotPath.size() - 1; i++)
            instructions.add(robotPath.get(i).createInstruction(0));

        // ainsi que le dernier en checkpoint final
        instructions.add(robotPath.get(robotPath.size()-1).createInstruction(1));

        instructions.add(new Instruction(Instruction.Type.DETECTION_AVANT, 0));

        int sens = pince.getType() == Instruction.Type.CYL_PINCE ? 1 : -1;

        // On s'oriente vers la zone
        instructions.add(new Instruction(Instruction.Type.ORIENTATION, pince.getFaceRobot(),  coordonnees.x, coordonnees.y));

        instructions.add(new Instruction(Instruction.Type.DETECTION_AVANT, 1));

        // Ensuite on s'avance vers la zone
        float dist = (float)Math.sqrt(checkPointRobot.getPos().dist2(coordonnees));
        instructions.add(new Instruction(Instruction.Type.AVANCER, sens * dist));

        instructions.add(new Instruction(Instruction.Type.DETECTION_AVANT, 0));

        // On ouvre la pince
        instructions.add(new Instruction(pince.getType(), 0));

        instructions.add(new Instruction(Instruction.Type.DETECTION_ARRIERE, 1));

        // Ensuite on recule
        instructions.add(new Instruction(Instruction.Type.AVANCER, -1 * sens * dist));

        instructions.add(new Instruction(Instruction.Type.DETECTION_ARRIERE, 0));

        // Puis on referme la pince
        instructions.add(new Instruction(pince.getType(), 1));

        instructions.add(new Instruction(Instruction.Type.ACTION_FINIE));
    }

    @Override
    public float calculateCost() {
        ////TODO : Vrai calcul de coût
        //return objective.getPointsGagnes() / coordonnees.dist2(contexte.getRobot().getCoordonnees());

        float bonus = 0;

        if(element instanceof Gobelet) {
            bonus = -3000;
        }

        // On va approx le coût avec le cout de la distance
        return CheckPoint.totalPathLength(robotPath) + bonus;
    }

    @Override
    public void postAction() {
        contexte.getRobot().setCoordonnees(coordonnees);
        pince.setObject(null);

        //TODO: Générer surchage des zones ?
    }

    public boolean canBeDone() {
        return (pince != null);
    }

}
