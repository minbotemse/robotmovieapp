package com.emse.minbot.minbotmanager.network;

/**
 * Created by vincent on 01/05/15.
 */
public interface BluetoothDataReceiver {
    public void receiveData(String data);
}
