package com.emse.minbot.minbotmanager.ia.jeu;

import com.emse.minbot.minbotmanager.ia.jeu.plateau.Element;
import com.emse.minbot.minbotmanager.ia.jeu.plateau.ElementObjectif;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by victor on 07/05/15.
 */
public class Strategie {
    private Date date = new Date();
    private PlateauContexte context;

    private ArrayList<Action> actionsToDo;
    private Action currentAction;

    public Strategie(PlateauContexte context) {
        this.context = context;
        currentAction = null;

        genererActions();
    }

    /**
     * Génère les objectifs
     * @return
     */
    public void genererActions() {
        ArrayList<Action> elementActions;

        actionsToDo = new ArrayList<>();

        // Pour chaque élément, on crée un objectif
        // et pour chaque objectif, on crée une action
        for (Element element : context.getElements()) {
            if(element instanceof ElementObjectif) {
                // On demande à l'élément de créer les actions
                elementActions = ((ElementObjectif) element).createActions();
                actionsToDo.addAll(elementActions);
            }
        }
    }

    public Action prochaineActionAFaire (PlateauContexte contexte) {
        Action nextActionToDo = null;
        float minCost = Float.MAX_VALUE, cost;

        // On met a jour les liens entre les checkpoints
        contexte.getGrid().updateLinks(date.getTime());

        // On demande à chaque instruction de générer ses instructions et autre en fonction
        // du contexte
        for(Action action : actionsToDo) {
            action.processContext(contexte);
        }

        // Puis on calcule les coûts et on prend l'action la plus avantageuse
        for(Action action : actionsToDo) {
            if(action.isPreviousDone() && action.canBeDone()) {
                cost = action.calculateCost();
                if(cost < minCost) {
                    minCost = cost;
                    nextActionToDo = action;
                }
            }
        }

        currentAction = nextActionToDo;
        return currentAction;
    }

    public void actionEffectuee (boolean actionFinie) {
        if(actionFinie) {
            currentAction.isDone();
            currentAction.postAction();
            actionsToDo.remove(currentAction);
        }
    }

    public void disableAction (Action action) {
        actionsToDo.remove(action);
    }

    public ArrayList<Action> getActionsToDo() {
        return actionsToDo;
    }
}
