package com.emse.minbot.minbotmanager.ia;

import java.util.HashMap;

/**
 * Created by vincent on 02/05/15.
 */
public class Protocol {

    public static final HashMap<Instruction.Type, Character> commandDictionnary = new HashMap<>(20);
    public static final char separator = ';';
    public static final char endOfCommand = '!';

    public static void establishProtocol () {
        /*
        commandDictionnary.put(Instruction.Type.DEPLACEMENT, 'D');
        commandDictionnary.put(Instruction.Type.CHECKPOINT, 'C');
        commandDictionnary.put(Instruction.Type.ORIENTATION, 'O');
        commandDictionnary.put(Instruction.Type.AVANCER, 'A');
        commandDictionnary.put(Instruction.Type.CYL_PINCE, 'Y');
        commandDictionnary.put(Instruction.Type.GOB_PINCE, 'G');
        commandDictionnary.put(Instruction.Type.BARRE_CLAPET, 'L');
        commandDictionnary.put(Instruction.Type.BARRE_POP, 'P');
        commandDictionnary.put(Instruction.Type.DETECTION_PINCE_CYL, 'T');
        commandDictionnary.put(Instruction.Type.DETECTION_PINCE_GOB, 'B');
        commandDictionnary.put(Instruction.Type.MODE_AUTOMATIQUE, 'U');
        commandDictionnary.put(Instruction.Type.MODE_MANUEL, 'M');
        commandDictionnary.put(Instruction.Type.DETECTION_AVANT, 'V');
        commandDictionnary.put(Instruction.Type.DETECTION_ARRIERE, 'R');
        commandDictionnary.put(Instruction.Type.ACTION_FINIE, 'I');
        */
        commandDictionnary.put(Instruction.Type.TOURNER, 'O');
        commandDictionnary.put(Instruction.Type.TOUR, 'T');
        commandDictionnary.put(Instruction.Type.VITESSE, 'V');
        commandDictionnary.put(Instruction.Type.STOP, 'S');
        commandDictionnary.put(Instruction.Type.PINCE, 'P');
        commandDictionnary.put(Instruction.Type.DETECTION, 'D');

    }

    public static char getCommand (Instruction.Type type) {
        return commandDictionnary.get(type);
    }

}
