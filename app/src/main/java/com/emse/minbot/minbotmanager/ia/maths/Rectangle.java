package com.emse.minbot.minbotmanager.ia.maths;

/**
 * Created by vincent on 11/05/15.
 */
public class Rectangle {
    // Coin haut gauche
    public Vector2D corner;
    // Taille
    public Vector2D size;

    public Rectangle(Vector2D corner, Vector2D size) {
        this.corner = corner;
        this.size = size;
    }

    public Rectangle(Vector2D topLeftCorner, Vector2D topRightCorner, Vector2D bottomRightCorner, Vector2D bottomLeftCorner) {
        this.corner = topLeftCorner;
        this.size = new Vector2D(topRightCorner.x - corner.x, bottomLeftCorner.y - corner.y);
    }

    public Vector2D closestPoint (Vector2D point) {
        float dx = 0, dy = 0;

        if(point.x < corner.x)
            dx = corner.x - point.x;
        else if(point.x > corner.x + size.x)
            dx = - (point.x - corner.x - size.x);
        if(point.y < corner.y)
            dy = corner.y - point.y;
        else if(point.y > corner.y + size.y)
            dy = - (point.y - corner.y - size.y);

        return new Vector2D(point.x + dx, point.y + dy);
    }

}
