package com.emse.minbot.minbotmanager.utilities;

import android.graphics.Point;

import java.util.ArrayList;

/**
 * Created by vincent on 02/05/15.
 */
public class Joystick {

    private int seuil;
    private ArrayList<JoystickButton> buttons;
    private int centreX, centreY;

    public Joystick (int seuil) {
        this.seuil = seuil;

        buttons = new ArrayList<>(4);
        buttons.add(new JoystickButton(new Point(55, 110), 'G'));
        buttons.add(new JoystickButton(new Point(125, 200), 'B'));
        buttons.add(new JoystickButton(new Point(190, 118), 'D'));
        buttons.add(new JoystickButton(new Point(132, 61), 'H'));

        centreX = 120;
        centreY = 110;
    }

    public char getDirection (int pX, int pY) {
        for(JoystickButton button : buttons)
            if(button.touches(new Point(pX, pY), seuil))
                return button.direction;

        return 'S'; // S pour STAND
    }

    public float[] get360Direction (int pX, int pY) {
        float cos, sin;
        float dist, dx, dy;

        dx = pX - centreX;
        dy = pY - centreY;
        dist = (float) Math.sqrt(dx*dx + dy*dy);

        cos = dx / dist;
        sin = dy / dist;

        return new float [] {cos, sin};
    }


    private class JoystickButton {
        private Point point;
        private char direction;

        public JoystickButton (Point point, char direction) {
            this.point = point;
            this.direction = direction;
        }

        public boolean touches(Point p, int seuil) {
            return ((p.x - point.x)*(p.x - point.x) + (p.y - point.y)*(p.y - point.y)) < seuil * seuil;
        }

        public int getX() {
            return point.x;
        }

        public int getY() {
            return point.y;
        }

    }


}
