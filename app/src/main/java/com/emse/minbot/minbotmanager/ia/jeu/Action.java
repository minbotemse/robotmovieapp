package com.emse.minbot.minbotmanager.ia.jeu;

import com.emse.minbot.minbotmanager.ia.Instruction;

import java.util.ArrayList;

/**
 * Created by victor on 07/05/15.
 */
public abstract class Action {
    protected Objectif objective;

    protected ArrayList<Instruction> instructions;
    protected int instructionEnCours = 0;

    // Action précédente nécessaire
    private Action previous = null;
    private boolean isDone = false;

    public Action(Objectif objectif) {
        this.objective = objectif;
        this.instructions = new ArrayList<>();
    }

    public Objectif getObjective() {
        return objective;
    }

    /**
     * Permet de calculer le cout, les parametres, etc de l'action dans un certain contexte
     * Ce cout sera garde en memoire jusqu'au prochain appel de la methode
     * L'interet est de simplifier les operations ulterieures sur l'action
     * @param contexte
     */
    public abstract void processContext(PlateauContexte contexte);

    /**
     * Calcule le coût total de l'action
     * @return
     */
    public abstract float calculateCost();

    /**
     * Effectue les changements au contexte post-action
     */
    public abstract void postAction();

    /**
     Savoir si l'action est faisable
     */
    public abstract boolean canBeDone();

    public boolean isPreviousDone () {
        return previous == null || previous.isDone;
    }

    public void isDone () {
        isDone = true;
    }

    public ArrayList<Instruction> getInstructions() {
        return instructions;
    }

    public Action getPrevious() {
        return previous;
    }

    public void setPrevious(Action previous) {
        this.previous = previous;
    }

}
