package com.emse.minbot.minbotmanager.ia.jeu;

import com.emse.minbot.minbotmanager.ia.Instruction;
import com.emse.minbot.minbotmanager.ia.algo.AStar;
import com.emse.minbot.minbotmanager.ia.jeu.plateau.BarreRotative;
import com.emse.minbot.minbotmanager.ia.jeu.plateau.Clapet;

import java.util.ArrayList;

/**
 * Created by victor on 10/05/15.
 */
public class ActionFermerClapet extends Action {
    private PlateauContexte contexte;
    private Clapet clapet;
    private ArrayList<CheckPoint> robotPath;

    public ActionFermerClapet(Objectif objectif, Clapet clapet) {
        super(objectif);
        this.clapet = clapet;
    }

    @Override
    public void processContext(PlateauContexte contexte) {
        instructions.clear();
        this.contexte = contexte;

        CheckPoint checkPointRobot = contexte.getGrid().getClosest(contexte.getRobot().getCoordonnees());
        CheckPoint checkPointElement = contexte.getGrid().getClosest(clapet.getCoordonnees());

        // On génère ensuite le chemin pour aller du robot à l'élément
        robotPath = AStar.FindPath(checkPointRobot, checkPointElement);

        // Si l'IA ne trouve pas de chemin, on passe
        if (robotPath == null)
            return;

        instructions.add(new Instruction(Instruction.Type.DETECTION_AVANT, 1));

        // On ajoute tous les checkpoints intermédiaires
        for(int i = 0; i < robotPath.size() - 1; i++)
            instructions.add(robotPath.get(i).createInstruction(0));

        // ainsi que le dernier en checkpoint final
        instructions.add(checkPointElement.createInstruction(1));

        instructions.add(new Instruction(Instruction.Type.DETECTION_AVANT, 0));

        // On oriente le robot vers le clapet
        instructions.add(new Instruction(Instruction.Type.ORIENTATION, 0, clapet.getCoordonnees().x, clapet.getCoordonnees().y));

        instructions.add(new Instruction(Instruction.Type.DETECTION_AVANT, 1));

        // On avance le robot
        float dist = (float)Math.sqrt(checkPointElement.getPos().dist2(clapet.getCoordonnees()));
        // mais pas trop...
        dist *= 0.80f;
        instructions.add(new Instruction(Instruction.Type.AVANCER, dist));

        instructions.add(new Instruction(Instruction.Type.DETECTION_AVANT, 0));

        // On oriente la face du robot avec la barre rotative vers le clapet
        BarreRotative barre = null;
        for (BarreRotative barreTempo : contexte.getRobot().getBarres())
            if(barreTempo.getType() == Instruction.Type.BARRE_CLAPET)
                barre = barreTempo;

        instructions.add(new Instruction(Instruction.Type.ORIENTATION, barre.getFaceRobot(), clapet.getCoordonnees().x, clapet.getCoordonnees().y));

        // On active la barre
        instructions.add(new Instruction(Instruction.Type.BARRE_CLAPET, 1));

        // On désactive la barre
        instructions.add(new Instruction(Instruction.Type.BARRE_CLAPET, 0));

        instructions.add(new Instruction(Instruction.Type.ACTION_FINIE));
    }

    @Override
    public float calculateCost() {
        // On va approx le coût avec le cout de la distance
        return CheckPoint.totalPathLength(robotPath);
    }

    @Override
    public void postAction() {
        contexte.getRobot().setCoordonnees(robotPath.get(robotPath.size()-1).getPos());
    }

    public boolean canBeDone() {
        return (robotPath != null);
    }

}
