package com.emse.minbot.minbotmanager.ia.jeu.plateau;

import com.emse.minbot.minbotmanager.ia.jeu.Action;
import com.emse.minbot.minbotmanager.ia.jeu.ActionBallesDuDistributeur;
import com.emse.minbot.minbotmanager.ia.jeu.ActionPoser;
import com.emse.minbot.minbotmanager.ia.jeu.ActionPrendre;
import com.emse.minbot.minbotmanager.ia.jeu.Objectif;
import com.emse.minbot.minbotmanager.ia.maths.Rectangle;
import com.emse.minbot.minbotmanager.ia.maths.Vector2D;

import java.util.ArrayList;

/**
 * Created by victor on 07/05/15.
 */
public class Gobelet extends ElementAttrapable {

    // ArrayList des salles de cinema
    public static final Zone ZONE =  new Zone(
            new Rectangle(new Vector2D(2669, 730), new Vector2D(2940, 730), new Vector2D(2940, 460), new Vector2D(2669, 460)),
            new Rectangle(new Vector2D(2640, 1570), new Vector2D(2940, 1570), new Vector2D(2940, 1264), new Vector2D(2640, 1264)));

    public Gobelet(Vector2D coordonnees) {
        super(coordonnees);
    }

    public Gobelet clone() { return new Gobelet(getCoordonnees()); }

    public ArrayList<Action> createActions() {
        ArrayList<Action> actions = new ArrayList<>();
        Objectif o = new Objectif(2, this);

        actions.add(new ActionPrendre(o, this));
        actions.add(new ActionBallesDuDistributeur(o, this));
        actions.add(new ActionPoser(o, this, ZONE));

        // Precedences
        int i;
        for (i=1; i < actions.size(); i++) {
            actions.get(i).setPrevious(actions.get(i - 1));
        }

        return actions;
    }
}
