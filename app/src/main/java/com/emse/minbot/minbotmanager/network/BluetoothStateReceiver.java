package com.emse.minbot.minbotmanager.network;

/**
 * Created by vincent on 02/05/15.
 */
public interface BluetoothStateReceiver {
    public void initialized();
    public void connectionEstablished ();
    public void noticeProgress(String info, int percentage);
    public void connectionLost(String reason);
}
