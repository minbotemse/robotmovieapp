package com.emse.minbot.minbotmanager.ia.jeu.plateau;

import com.emse.minbot.minbotmanager.ia.jeu.Action;
import com.emse.minbot.minbotmanager.ia.maths.Vector2D;

import java.util.ArrayList;

/**
 * Created by vincent on 11/05/15.
 */
public abstract class ElementObjectif extends Element {

    public ElementObjectif(Vector2D coordonnees) {
        super(coordonnees);
    }

    public abstract ArrayList<Action> createActions();
}
