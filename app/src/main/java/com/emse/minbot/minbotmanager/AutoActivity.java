package com.emse.minbot.minbotmanager;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;

import com.emse.minbot.minbotmanager.ia.Instruction;
import com.emse.minbot.minbotmanager.ia.Jarvis;
import com.emse.minbot.minbotmanager.ia.jeu.Afterwork2015;
import com.emse.minbot.minbotmanager.ia.utils.MoodListener;
import com.emse.minbot.minbotmanager.network.BluetoothDataReceiver;
import com.emse.minbot.minbotmanager.network.BluetoothManager;
import com.emse.minbot.minbotmanager.network.BluetoothStateReceiver;
import com.emse.minbot.minbotmanager.utilities.RobotVoice;
import com.emse.minbot.minbotmanager.utilities.RoboticEyes;


public class AutoActivity extends Activity implements BluetoothDataReceiver, BluetoothStateReceiver, MoodListener {

    private boolean mBound = false;
    private Jarvis jarvis;
    private RoboticEyes eyes;
    private RobotVoice voice;
    //private CoupeRobotique2015.Couleur couleur;
    private Afterwork2015.IAType iaType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auto);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String value = extras.getString("IAType");
            if (value.equals("AGRESSIF"))
                iaType = Afterwork2015.IAType.AGRESSIF;
            else
                iaType = Afterwork2015.IAType.PEUREUX;
            /*
            if(value.equals("VERT"))
                couleur = CoupeRobotique2015.Couleur.VERT;
            else
                couleur = CoupeRobotique2015.Couleur.JAUNE;
                */
        }

        eyes = (RoboticEyes) findViewById(R.id.roboticEyesView);

        BluetoothManager.registerDataListener(this);
        BluetoothManager.registerStateListener(this);

        if(!mBound)
            bindIA();

        voice = new RobotVoice(this.getApplicationContext());
        voice.create(this);
    }

    private void adaptMood (final Mood mood) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch(mood) {
                    case ANXIOUS:
                        eyes.changeEyesProperties(0xffE17E3B, 5);
                        break;
                    case NO_RESPONSE:
                        eyes.changeEyesProperties(0xff1D6F46, 3);
                        break;
                    case OK:
                        eyes.changeEyesProperties(0xff5091C2, 10);
                        break;
                    case SLEEPING:
                        eyes.changeEyesProperties(0xff761CB2, 1);
                        break;
                    case SURPRISED:
                        eyes.changeEyesProperties(0xffC12828, -20);
                        break;
                }
            }
        });
    }

    private void bindIA () {
        if(!mBound) {
            Intent intent = new Intent(this, Jarvis.class);
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        }
    }

    private void IAConnected () {
        jarvis.start(iaType);
        adaptMood(Mood.SLEEPING);
        voice.speakWords("Wake me when the war has started.");
        BluetoothManager.sendData(new Instruction(Instruction.Type.MODE_AUTOMATIQUE).generateCommand());
    }

    @Override
    protected void onStop(){
        super.onStop();

        jarvis.stopIA();

        if(mBound) {
            unbindService(mConnection);
            mBound = false;
        }

        eyes.stopDrawing();
        voice.stop();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        voice.onActivityResult(this, requestCode, resultCode, data);
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            Jarvis.IALocalBinder binder = (Jarvis.IALocalBinder) service;
            jarvis = binder.getJarvis();
            jarvis.setMoodListener(AutoActivity.this);
            mBound = true;

            IAConnected();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    public void receiveData(String data) {
        jarvis.messageFromRobot(data);
    }

    @Override
    public void initialized() {
        IAConnected();
    }

    @Override
    public void connectionEstablished() {
        adaptMood(Mood.SLEEPING);
    }

    @Override
    public void noticeProgress(String info, int percentage) {}

    @Override
    public void connectionLost(String reason) {
        adaptMood(Mood.NO_RESPONSE);
        voice.speakWords("Connexion lost! DOCTOR, WHERE ARE YOU?!");
        BluetoothManager.getBluetoothManager().endConnection();
        BluetoothManager.getBluetoothManager().initializeConnection();
    }

    @Override
    public void moodChanged(Mood mood) {
        adaptMood(mood);
    }
}
