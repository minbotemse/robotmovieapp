package com.emse.minbot.minbotmanager.ia.jeu.plateau;

import com.emse.minbot.minbotmanager.ia.Instruction;

/**
 * Created by victor on 07/05/15.
 */
public class Pince extends Peripherique {

    private ElementAttrapable objet;

    public Pince(Instruction.Type instruction, int face) {
        super(instruction, face);
        objet = null;
    }

    public void setObject(ElementAttrapable objet) {
        this.objet = objet;
    }

    public ElementAttrapable getObjet() {
        return objet;
    }
    public boolean isEmpty() {
        return (objet == null);
    }

}
