package com.emse.minbot.minbotmanager.network;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.emse.minbot.minbotmanager.ia.Protocol;

public class BluetoothManager extends Service {

    private static BluetoothManager bluetoothManager = null;
    private final BluetoothLocalBinder mBinder = new BluetoothLocalBinder();

    private BluetoothConnection connectionManager;

    public BluetoothManager() {
        bluetoothManager = this;
    }

    @Override
    public void onCreate() {
        bluetoothManager = this;
        connectionManager = new BluetoothConnection();

        Protocol.establishProtocol();
    }

    public static void registerDataListener (BluetoothDataReceiver receiver) {
        getBluetoothManager().connectionManager.connectDataReceiver(receiver);
    }

    public static void registerStateListener (BluetoothStateReceiver receiver) {
        getBluetoothManager().connectionManager.connectStateReceiver(receiver);
    }

    public String initializeConnection () {
        return connectionManager.initialize();
    }

    public void startConnection () {
        connectionManager.connect();
    }

    public void endConnection () {connectionManager.close(); }

    public static void sendData (String data) {
        getBluetoothManager().connectionManager.writeData(data);
    }

    public static boolean isConnected () {
        return getBluetoothManager().connectionManager.isConnected();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class BluetoothLocalBinder extends Binder {
        public BluetoothManager getBluetoothManager () {
            return BluetoothManager.this;
        }
    }

    public static BluetoothManager getBluetoothManager() {
        return bluetoothManager;
    }
}
