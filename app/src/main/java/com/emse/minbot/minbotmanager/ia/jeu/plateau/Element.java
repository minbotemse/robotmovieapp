package com.emse.minbot.minbotmanager.ia.jeu.plateau;

import com.emse.minbot.minbotmanager.ia.maths.Vector2D;

/**
 * Created by victor on 07/05/15.
 */
public abstract class Element {
    Vector2D coordonnees;

    public Element(Vector2D coordonnees) {
        this.coordonnees = coordonnees;
    }

    public Vector2D getCoordonnees() {
        return coordonnees;
    }

    public void setCoordonnees(Vector2D coordonnees) {
        this.coordonnees = coordonnees;
    }

    public String toString () {
        return "\u001B[31m" + "\033[0;1m" + getClass().getSimpleName() + "\033[0;0m " + coordonnees;
    }

}
