package com.emse.minbot.minbotmanager.ia.jeu;

import com.emse.minbot.minbotmanager.ia.Instruction;
import com.emse.minbot.minbotmanager.ia.jeu.plateau.BarreRotative;
import com.emse.minbot.minbotmanager.ia.jeu.plateau.Clapet;
import com.emse.minbot.minbotmanager.ia.jeu.plateau.Cylindre;
import com.emse.minbot.minbotmanager.ia.jeu.plateau.Distributeur;
import com.emse.minbot.minbotmanager.ia.jeu.plateau.Element;
import com.emse.minbot.minbotmanager.ia.jeu.plateau.Gobelet;
import com.emse.minbot.minbotmanager.ia.jeu.plateau.Pince;
import com.emse.minbot.minbotmanager.ia.jeu.plateau.Robot;
import com.emse.minbot.minbotmanager.ia.maths.Vector2D;

import java.util.ArrayList;

/**
 * Created by vincent on 12/05/15.
 */
public class CoupeRobotique2015 {

    public enum Couleur{JAUNE, VERT}

    public static int setX(int valeur, Couleur couleur) {
        if (couleur==Couleur.JAUNE)
            return valeur;
        else
            return 3000-valeur;
    }

    public static PlateauContexte genererPlateauContexte (Couleur couleur) {

        // On definit le robot
        Pince pinces[] = {
                new Pince(Instruction.Type.CYL_PINCE, 0),
                new Pince(Instruction.Type.GOB_PINCE, 3)
        };

        BarreRotative barres[] = {
                new BarreRotative(Instruction.Type.BARRE_CLAPET, 1),
                new BarreRotative(Instruction.Type.BARRE_POP, 3)
        };

        Robot robot = new Robot(new Vector2D(300,1000), pinces, barres);
        // On definit les elements du plateau
        ArrayList<Element> elements = new ArrayList<>();
        elements.add(new Gobelet(new Vector2D(setX(900, couleur),830)));
        elements.add(new Gobelet(new Vector2D(setX(240, couleur),1750)));
        elements.add(new Gobelet(new Vector2D(setX(1500, couleur),1650)));
        elements.add(new Cylindre(new Vector2D(setX(90, couleur),1850)));
        elements.add(new Cylindre(new Vector2D(setX(850, couleur),100)));
        elements.add(new Cylindre(new Vector2D(setX(850, couleur),200)));
        elements.add(new Cylindre(new Vector2D(setX(1100, couleur),1800)));
        elements.add(new Cylindre(new Vector2D(setX(830, couleur),1400)));
        elements.add(new Cylindre(new Vector2D(setX(1300, couleur),1400)));
        elements.add(new Cylindre(new Vector2D(setX(80, couleur),1700)));
        elements.add(new Cylindre(new Vector2D(setX(80, couleur),190)));
        elements.add(new Distributeur(new Vector2D(setX(2400, couleur),40)));
        elements.add(new Distributeur(new Vector2D(setX(2700, couleur),40)));
        elements.add(new Distributeur(new Vector2D(setX(300, couleur),40)));
        elements.add(new Distributeur(new Vector2D(setX(600, couleur),40)));
        elements.add(new Clapet(new Vector2D(setX(250, couleur),1970)));
        elements.add(new Clapet(new Vector2D(setX(850, couleur),1970)));
        elements.add(new Clapet(new Vector2D(setX(2450, couleur),1970)));
        PlateauContexte contexte = new PlateauContexte(robot, elements, new Grid());

        return contexte;
    }
}
