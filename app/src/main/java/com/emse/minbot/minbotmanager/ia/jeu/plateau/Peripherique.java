package com.emse.minbot.minbotmanager.ia.jeu.plateau;

import com.emse.minbot.minbotmanager.ia.Instruction;

/**
 * Created by vincent on 12/05/15.
 */
public class Peripherique {

    private Instruction.Type type;
    private int faceRobot;

    public Peripherique(Instruction.Type type, int faceRobot) {
        this.type = type;
        this.faceRobot = faceRobot;
    }

    public Instruction.Type getType() {
        return type;
    }

    public void setType(Instruction.Type type) {
        this.type = type;
    }

    public int getFaceRobot() {
        return faceRobot;
    }

    public void setFaceRobot(int faceRobot) {
        this.faceRobot = faceRobot;
    }
}
