package com.emse.minbot.minbotmanager.ia.jeu;

import com.emse.minbot.minbotmanager.ia.Instruction;
import com.emse.minbot.minbotmanager.ia.algo.AStarCell;
import com.emse.minbot.minbotmanager.ia.maths.Vector2D;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by vincent on 02/05/15.
 */
public class CheckPoint implements AStarCell {
    public static final int LINKFORBIDDINGCOOLDOWN = 10;
    private Vector2D pos;
    private ArrayList<AStarCell> allNeighbours = new ArrayList<>();
    private HashMap<AStarCell, Long> neighboursForbidDates = new HashMap<>();
    /**
     * ValidNeighbours doit etre remis a jour regulierement afin de prendre en compte les cooldowns des voisins interdits
     */
    private ArrayList<AStarCell> validNeighbours = new ArrayList<>();

    public CheckPoint(int x, int y) {
        this.pos = new Vector2D(x, y);
    }

    public void connect(CheckPoint neighbour) {
        allNeighbours.add(neighbour);
    }

    /**
     * Interdire un voisin
     * @param neighbour
     * @param date La date actuelle
     */
    public void forbid(CheckPoint neighbour, long date) {
        neighboursForbidDates.put(neighbour, date);
    }

    /**
     * Mettre a jour les voisins autorises
     * @param date
     */
    public void updateLinks(long date) {
        validNeighbours.clear();
        for (AStarCell neighbour: allNeighbours) {
            if (neighboursForbidDates.containsKey(neighbour))
                if (neighboursForbidDates.get(neighbour) - date < LINKFORBIDDINGCOOLDOWN)
                    continue;
                else
                    neighboursForbidDates.remove((CheckPoint)neighbour);
            validNeighbours.add(neighbour);
        }
    }

    @Override
    public ArrayList<AStarCell> getNeighbors() {
        return validNeighbours;
    }

    @Override
    public int distanceTo(AStarCell cell) {
        //return (int)Math.sqrt(((CheckPoint) cell).distanceTo(this));
        CheckPoint checkPoint = (CheckPoint) cell;
        return (int)Math.sqrt(checkPoint.getPos().dist2(pos));
    }

    @Override
    public int heuristicDistanceTo(AStarCell cell) {
        return (int)Math.sqrt(((CheckPoint) cell).distanceTo(this));
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;

        if(! (o instanceof CheckPoint))
            return false;

        CheckPoint c = (CheckPoint) o;

        return (c.pos.x == pos.x && c.pos.y == pos.y);
    }

    public Instruction createInstruction(int stopChar) {
        return new Instruction(Instruction.Type.CHECKPOINT,
                pos.x, pos.y, stopChar);
    }

    public Vector2D getPos() {
        return pos;
    }

    static public float totalPathLength(ArrayList<CheckPoint> path) {
        int i;
        float length = 0;
        for (i=1; i<path.size(); i++) {
            length = length + path.get(i).distanceTo(path.get(i-1));
        }
        return length;
    }
}
