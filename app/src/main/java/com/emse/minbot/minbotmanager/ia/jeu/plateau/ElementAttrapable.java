package com.emse.minbot.minbotmanager.ia.jeu.plateau;

import com.emse.minbot.minbotmanager.ia.maths.Vector2D;

/**
 * Created by victor on 07/05/15.
 */


public abstract class ElementAttrapable extends ElementObjectif {

    public ElementAttrapable(Vector2D coordonnees) {
        super(coordonnees);
    }
}
