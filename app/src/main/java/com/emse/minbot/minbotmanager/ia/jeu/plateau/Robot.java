package com.emse.minbot.minbotmanager.ia.jeu.plateau;


import com.emse.minbot.minbotmanager.ia.maths.Vector2D;

/**
 * Created by victor on 07/05/15.
 */
public class Robot extends Element {
    private Pince pinces[];
    private BarreRotative barres[];

    public Robot(Vector2D coordonnees, Pince pinces[], BarreRotative[] barres) {
        super(coordonnees);
        this.pinces = pinces;
        this.barres = barres;
    }

    public Pince[] getPinces() {
        return pinces;
    }

    public void setPinces(Pince[] pinces) {
        this.pinces = pinces;
    }

    public BarreRotative[] getBarres() {
        return barres;
    }

    public void setBarres(BarreRotative[] barres) {
        this.barres = barres;
    }
}
