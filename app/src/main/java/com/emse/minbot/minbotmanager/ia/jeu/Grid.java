package com.emse.minbot.minbotmanager.ia.jeu;

import com.emse.minbot.minbotmanager.ia.maths.Vector2D;

import java.util.ArrayList;

/**
 * Created by vincent on 03/05/15.
 */
public class Grid {

    private ArrayList<CheckPoint> grid = null;

    private int[][] checkpoints = {
        {776,1807}, //G
        {1034,1549}, //M
        {2348,1007},
        {1479,984},
        {2839,1439},
        {625,515},
        {2279,1848}, //D
        {308,993}, //S
        {1475,1228},
        {2421,1582},
        {583,1559},
        {1686,736},
        {1994,1549},
        {2329,474},
        {1874,1131},
        {386,285}, //H
        {1103,1140},
        {2536,267},
        {1250,731},
        {1231,179}, //E
        {652,993},
        {2802,566}
    };

    private int connexions [][] = {
        {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
        {0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0},
        {0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
        {0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1},
        {0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
        {0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0}
    };

    public Grid () {
        constructGrid();
    }

    private void constructGrid () {
        grid = new ArrayList<>();

        for(int[] coordinates : checkpoints) {
            grid.add(new CheckPoint(coordinates[0], coordinates[1]));
        }

        for(int i = 0; i < connexions.length; i++)
            for(int j = 0; j < connexions[0].length; j++)
                if(connexions[i][j] == 1) {
                    grid.get(i).connect(grid.get(j));
                    grid.get(j).connect(grid.get(i));
                }
    }

    public CheckPoint getClosest(Vector2D point) {
        CheckPoint closest = grid.get(0);
        float dist, distMin = Float.MAX_VALUE;

        for(CheckPoint checkPoint : grid) {
            dist = checkPoint.getPos().dist2(point);
            if(dist < distMin) {
                distMin = dist;
                closest = checkPoint;
            }
        }

        return closest;
    }

    /**
     * Mettre a jour les liens entre les checkpoints autorises
     * @param date La date actuelle
     */
    public void updateLinks(long date) {
        for (CheckPoint checkPoint: grid) {
            checkPoint.updateLinks(date);
        }
    }
}
