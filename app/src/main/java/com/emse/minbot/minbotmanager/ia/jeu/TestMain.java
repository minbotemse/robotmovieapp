package com.emse.minbot.minbotmanager.ia.jeu;

import com.emse.minbot.minbotmanager.ia.Instruction;
import com.emse.minbot.minbotmanager.ia.Protocol;

import java.util.Collections;

public class TestMain {

    // classe de test pour le fonctionnement de la strategie

    public static void main(String[] args) {

        PlateauContexte contexte = CoupeRobotique2015.genererPlateauContexte(CoupeRobotique2015.Couleur.VERT);

        // On definit et execute une strategie
        Strategie strategie = new Strategie(contexte);
        Action action;

        Protocol.establishProtocol();

        strategie.genererActions();
        Collections.shuffle(strategie.getActionsToDo());

        do {
            System.out.println("================== CONTEXTE =======================");
            System.out.println(contexte.getRobot());
            System.out.println("Pinces : p1 contient " + contexte.getRobot().getPinces()[0].getObjet() + ", p2 contient " + contexte.getRobot().getPinces()[1].getObjet());
            System.out.println("Il reste " + strategie.getActionsToDo().size() + " actions.");
            System.out.println("------------------------");

            action = strategie.prochaineActionAFaire(contexte);

            if (action != null) {
                System.out.println("Action sélectionnée : " + action.getClass().getSimpleName());
                for (Instruction instruction : action.getInstructions())
                    System.out.println("\t - " + instruction.generateCommand());
                strategie.actionEffectuee(true);
            }
        }
        while(action != null);

        System.out.println("================== CONTEXTE FINAL =======================");
        System.out.println(contexte.getRobot());
        System.out.println("Pinces : p1 contient " + contexte.getRobot().getPinces()[0].getObjet() + ", p2 contient " + contexte.getRobot().getPinces()[1].getObjet());
        System.out.println("Il reste " + strategie.getActionsToDo().size() + " actions.");

        System.out.println("La simulation a rapporté " + contexte.getScore() + " points (durée: " + contexte.getDuration() + ").");
    }
}
