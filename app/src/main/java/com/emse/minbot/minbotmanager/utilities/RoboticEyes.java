package com.emse.minbot.minbotmanager.utilities;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by vincent on 07/05/15.
 */
public class RoboticEyes extends SurfaceView implements Runnable {

    private SurfaceHolder holder;
    private Eye[] eyes = new Eye[2];

    private Thread thread;
    private boolean running = false;
    //for consistent rendering
    private long sleepTime;
    //amount of time to sleep for (in milliseconds)
    private long delay=10;

    public RoboticEyes(Context context) {
        super(context);
        initView();
    }

    public RoboticEyes(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public RoboticEyes(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        this.setWillNotDraw(false);

        holder = getHolder();
        holder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                stopDrawing();
                boolean done = false;
                while(!done) {
                    try {
                        thread.join();
                        done = true;
                    }
                    catch (InterruptedException e) {
                    }
                }
            }

            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                running = true;
                thread = new Thread(RoboticEyes.this);
                thread.start();
            }

            @Override

            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

        });

        eyes[0] = new Eye(true);
        eyes[1] = new Eye(false);

        Log.i("MINBOT", "Created!");
    }

    public void adaptEyes() {

        int min = getHeight() < getWidth() ? getHeight() : getWidth();

        eyes[0].adaptEyes(getWidth() / 4, getHeight() / 2, min / 20, min*3/7);
        eyes[1].adaptEyes(getWidth() - getWidth() / 4, getHeight() / 2, min / 20, min*3/7);
    }

    public void changeEyesProperties (int color, int speed) {
        eyes[0].changeColor(color);
        eyes[0].changeSpeed(speed);
        eyes[1].changeColor(color);
        eyes[1].changeSpeed(speed);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawColor(Color.BLACK);

        for(Eye eye : eyes)
            eye.draw(canvas);
    }

    public void stopDrawing () {
        running = false;
    }

    private void update () {
        for (Eye eye : eyes)
            eye.rotateEyes();
    }

    @Override
    public void run() {
        adaptEyes();
        running = true;

        while (running) {

            long beforeTime = System.nanoTime();

            update();

            Canvas canvas = null;

            try {
                canvas = getHolder().lockCanvas();
                synchronized (getHolder()) {
                    postInvalidate();
                }

            }  catch (Exception e) {
                e.printStackTrace();
            }
            finally {
                if (canvas != null) {
                    getHolder().unlockCanvasAndPost(canvas);
                }
            }

            this.sleepTime = delay-((System.nanoTime()-beforeTime)/1000000L);

            try {
                if(sleepTime>0){
                    Thread.sleep(sleepTime);
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(RoboticEyes.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private class Eye {
        private final int nbLines = 6;
        private int strokeLength;
        private RectF[] rectangles = new RectF[nbLines];
        private float[] startAngles = new float[nbLines];
        private float speed = 3f;
        private boolean initialized = false;
        private boolean left = false;

        private Paint eyePaint;

        public Eye (boolean left) {
            this.left = left;
        }

        public void adaptEyes (int cx, int cy, int rmin, int rmax) {
            int r;

            for(int i = 0; i < nbLines; i++) {
                r = rmin + i*(rmax - rmin)/nbLines;
                rectangles[i] = new RectF(cx - r, cy - r, cx + r, cy + r);
                if(left)
                    startAngles[i] = 0;
                else
                    startAngles[i] = 0;
            }

            strokeLength = ((rmax - rmin)/nbLines)*3/4;

            eyePaint = new Paint();
            eyePaint.setAntiAlias(true);
            eyePaint.setStyle(Paint.Style.STROKE);
            eyePaint.setColor(Color.DKGRAY);
            eyePaint.setStrokeWidth(strokeLength);

            initialized = true;
        }

        public void draw(Canvas canvas) {
            if(initialized) {
                for(int i = 0; i < nbLines; i++) {
                    if(left)
                        canvas.drawArc(rectangles[i], startAngles[i], 180, false, eyePaint);
                    else
                        canvas.drawArc(rectangles[i],startAngles[i], 180, false, eyePaint);
                }
            }
        }

        public void changeSpeed(float speed) {
            this.speed = speed;
        }

        public void changeColor (int color) {
            eyePaint.setColor(color);
        }

        public void rotateEyes () {
            for(int i = 0; i < nbLines; i++) {
                if(left)
                    startAngles[i] = (startAngles[i] + speed/(i+1))%360;
                else
                    startAngles[i] = (startAngles[i] - speed/(i+1) + 360)%360;
            }
        }
    }
}
