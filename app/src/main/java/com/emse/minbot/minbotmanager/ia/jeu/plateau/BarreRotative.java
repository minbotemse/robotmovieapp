package com.emse.minbot.minbotmanager.ia.jeu.plateau;

import com.emse.minbot.minbotmanager.ia.Instruction;

/**
 * Created by vincent on 12/05/15.
 */
public class BarreRotative extends Peripherique {

    public BarreRotative(Instruction.Type type, int faceRobot) {
        super(type, faceRobot);
    }
}
