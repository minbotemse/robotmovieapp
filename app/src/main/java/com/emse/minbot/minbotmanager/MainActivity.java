package com.emse.minbot.minbotmanager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.emse.minbot.minbotmanager.utilities.RobotVoice;

import at.markushi.ui.CircleButton;


public class MainActivity extends Activity {

    private CircleButton splashbutton = null;
    private RobotVoice voice;

    @Override
           public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        splashbutton = (CircleButton)findViewById(R.id.splashScreenButton);
        splashbutton.setOnClickListener(splashListener);

        voice = new RobotVoice(this.getApplicationContext());
        voice.create(this);
    };

    private View.OnClickListener splashListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //voice.speakWords("Wake me when the war has started.");
            //voice.speakWords("You freaking robot! work now please ,otherwise i call dolan bitches");
            //voice.speakWords("I'm just going by...");
            //voice.speakWords("I hate hard labour...");
            //voice.speakWords("Damn it! Where is it ?! I can't see a thing!");
            //voice.speakWords("EXTERMINATE! You won't get me!");
            //voice.speakWords("Connexion lost! DOCTOR, WHERE ARE YOU?!");
            //*
            Intent intent = new Intent(MainActivity.this, BluetoothActivity.class);
            startActivity(intent);
            //*/
        }
    };

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        voice.onActivityResult(this, requestCode, resultCode, data);
    }
}
