package com.emse.minbot.minbotmanager.ia.algo;

import java.util.ArrayList;


public interface AStarCell {
	
	public ArrayList<? extends AStarCell> getNeighbors();
	
	// Pour les voisins
	public int distanceTo(AStarCell cell);
	
	public int heuristicDistanceTo(AStarCell cell);
}
