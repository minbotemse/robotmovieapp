package com.emse.minbot.minbotmanager.ia.algo;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by victor on 17/04/15.
 */
public class AStar {
	public static <E extends AStarCell> ArrayList<E> FindPath(E start, E goal) {
		ArrayList<E> closedSet = new ArrayList<E>();
		ArrayList<E> openSet = new ArrayList<E>();
		openSet.add(start);
		HashMap<E, Integer> costTo = new HashMap<>();
		HashMap<E, Integer> costFrom = new HashMap<>();
		HashMap<E, E> precedingNode = new HashMap<>();
		
		precedingNode.put(start, null);

		costTo.put(start, 0);
		costFrom.put(start,
				costTo.get(start) + start.heuristicDistanceTo(goal));

		while (!openSet.isEmpty()) {
			// Find the node in openSet which has the lowest cost from the start
			E current = null;
			for (E cell: openSet) {
				if (current == null || costTo.get(current) < costTo.get(cell))
					current = cell;
			}
			
			if (current == goal) {
				return rebuildPath(precedingNode, goal);
			}
			
			openSet.remove(current);
			closedSet.add(current);
			
			for (AStarCell neighbor: current.getNeighbors()) {
				if (closedSet.contains(neighbor))
					continue;
				int costEstimate = costTo.get(current) + current.distanceTo(neighbor);
				
				if (!openSet.contains(neighbor) || costEstimate < costTo.get(neighbor)) {
					precedingNode.put((E) neighbor, current);
					costTo.put((E) neighbor, costEstimate);
					costFrom.put((E) neighbor, costEstimate + neighbor.heuristicDistanceTo(goal));
					if (!openSet.contains(neighbor)) {
						openSet.add((E) neighbor);
					}
				}
			}
			
		}

		return null;
	}
	
	private static <E extends AStarCell> ArrayList<E> rebuildPath(HashMap<E,E> precedingNode, E goal) {
		ArrayList<E> path = new ArrayList<E>();
		E current = goal;
		while (current != null)
		{
			path.add(0, current);
			current = precedingNode.get(current);
		}
		return path;
	}
}