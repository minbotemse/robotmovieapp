package com.emse.minbot.minbotmanager.ia.jeu.plateau;

import com.emse.minbot.minbotmanager.ia.jeu.Action;
import com.emse.minbot.minbotmanager.ia.jeu.ActionPoser;
import com.emse.minbot.minbotmanager.ia.jeu.ActionPrendre;
import com.emse.minbot.minbotmanager.ia.jeu.Objectif;
import com.emse.minbot.minbotmanager.ia.maths.Rectangle;
import com.emse.minbot.minbotmanager.ia.maths.Vector2D;

import java.util.ArrayList;

public class Cylindre extends ElementAttrapable {

    // ArrayList des zones de montage
    public static final Zone ZONE = new Zone(
            new Rectangle(new Vector2D(1120,1970), new Vector2D(1160,1970), new Vector2D(1160,1850), new Vector2D(1120,1850)),
            new Rectangle(new Vector2D(1210,1900), new Vector2D(1780,1900), new Vector2D(1780,1810), new Vector2D(1210,1810)),
            new Rectangle(new Vector2D(1810,1980), new Vector2D(1888,1980), new Vector2D(1888,1850), new Vector2D(1810,1850)),
            new Rectangle(new Vector2D(80,1177), new Vector2D(410,1177), new Vector2D(410,800), new Vector2D(80,800)),
            new Rectangle(new Vector2D(420,1126), new Vector2D(585,1126), new Vector2D(585,900), new Vector2D(420,900)));

    public Cylindre(Vector2D coordonnees) {
        super(coordonnees);
    }

    public ArrayList<Action> createActions() {
        ArrayList<Action> actions = new ArrayList<>();
        Objectif objective = new Objectif(2, this);

        actions.add(new ActionPrendre(objective, this));
        actions.add(new ActionPoser(objective, this, ZONE));

        // Précédences
        int i;
        for (i=1; i < actions.size(); i++) {
            actions.get(i).setPrevious(actions.get(i - 1));
        }

        return actions;
    }
}