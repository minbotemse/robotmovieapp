package com.emse.minbot.minbotmanager.ia.maths;


/**
 * Created by vincent on 02/05/15.
 */
public class Vector2D {
    public float x;
    public float y;

    public Vector2D(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public Vector2D(Vector2D vector2D) {
        this.x = vector2D.x;
        this.y = vector2D.y;
    }

    public float dist2 (Vector2D v) {
        return (v.x - x)*(v.x - x) + (v.y - y)*(v.y - y);
    }

    public Vector2D add (Vector2D v) {
        return new Vector2D(x + v.x, y + v.y);
    }

    public Vector2D by (float f) {
        return new Vector2D(x*f, y*f);
    }

    public float by (Vector2D v) {
        return x*v.x + y*v.y;
    }

    public float det (Vector2D v) {
        return x*v.y - y*v.x;
    }

    public float dist () {
        return (float)Math.sqrt(this.by(this));
    }

    public Vector2D unit () {
        return by(1/dist());
    }

    public float angleFromVectical () {
        if(y == 0 && x > 0)
            return (float)Math.PI/2;
        else
            return 2*(float)Math.atan(y/(x+dist())) - (float)Math.PI/2;
    }

    public String toString() {
        return "(" + x + ";" + y + ")";
    }
}
