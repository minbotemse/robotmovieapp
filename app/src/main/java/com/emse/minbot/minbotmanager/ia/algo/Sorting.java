package com.emse.minbot.minbotmanager.ia.algo;

import java.util.ArrayList;
import java.util.Collection;

public class Sorting {

	private static <T extends Comparable<T>> void siftDown(ArrayList<T> array,
			int start, int end) {
		int root = start;
		while (root * 2 + 1 <= end) {
			int child = root * 2 + 1;
			if (child + 1 <= end
					&& array.get(child).compareTo(array.get(child + 1)) < 0) {
				child++;
			}
			if (array.get(root).compareTo(array.get(child)) < 0) {
				T swap = array.get(child);
				array.set(child, array.get(root));
				array.set(root, swap);
				root = child;
			} else {
				return;
			}
		}
	}

	private static <T extends Comparable<T>> void heapify(ArrayList<T> array) {
		for (int start = array.size() / 2; start >= 0; start--) {
			siftDown(array, start, array.size() - 1);
		}
	}

	public static <T extends Comparable<T>> ArrayList<T> heapSort(
			Collection<T> input) {
		ArrayList<T> resultingArray = new ArrayList<>(input);
		heapify(resultingArray);
		for (int end = resultingArray.size() - 1; end > 0;) {
			T swap = resultingArray.get(0);
			resultingArray.set(0, resultingArray.get(end));
			resultingArray.set(end, swap);
			end -= 1;
			siftDown(resultingArray, 0, end);
		}
		return resultingArray;
	}

/*
	public static void main(String[] args) {
		ArrayList<SortableNumber> array = new ArrayList<>();
		array.add(new SortableNumber(13));
		array.add(new SortableNumber(3));
		array.add(new SortableNumber(1));
		array.add(new SortableNumber(9));
		array.add(new SortableNumber(3));
		array.add(new SortableNumber(8));
		array.add(new SortableNumber(5));
		array.add(new SortableNumber(0));
		for (int i = 0; i < array.size(); i++) {
			System.out.print(array.get(i) + ";");
		}
		System.out.println("");
		array = Sorting.heapSort(array);
		for (int i = 0; i < array.size(); i++) {
			System.out.print(array.get(i) + ";");
		}
		System.out.println("");
	}
	*/

}
