package com.emse.minbot.minbotmanager.ia.objectives;

/**
 * Created by vincent on 03/05/15.
 */
public abstract class Objective {

    private String name;

    public Objective (String name) {
        this.name = name;
    }

    @Override
    public String toString () {
        return name;
    }

}
