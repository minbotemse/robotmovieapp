package com.emse.minbot.minbotmanager.ia.jeu;

import com.emse.minbot.minbotmanager.ia.jeu.plateau.Element;

/**
 * Created by victor on 07/05/15.
 */
public class Objectif {
    private int pointsGagnes;
    private Element element;

    public Objectif(int pointsGagnes, Element element)
    {
        this.pointsGagnes = pointsGagnes;
        this.element = element;
    }

    public int getPointsGagnes()
    {
        return pointsGagnes;
    }

}
