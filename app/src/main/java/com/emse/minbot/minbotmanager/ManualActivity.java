package com.emse.minbot.minbotmanager;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.emse.minbot.minbotmanager.ia.Instruction;
import com.emse.minbot.minbotmanager.network.BluetoothDataReceiver;
import com.emse.minbot.minbotmanager.network.BluetoothManager;
import com.emse.minbot.minbotmanager.network.BluetoothStateReceiver;
import com.emse.minbot.minbotmanager.utilities.Joystick;


public class ManualActivity extends Activity implements View.OnTouchListener, BluetoothDataReceiver, BluetoothStateReceiver {

    private ImageView stickButton;
    private TextView console;
    private ScrollView scrollView;

    private Joystick joystick;
    private long timeThen;
    private boolean counting = false;
    private final long joystickCounterLimit = 100;

    private boolean joystickPressed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual);

        stickButton = (ImageView) findViewById(R.id.stick);
        stickButton.setOnTouchListener(this);

        console = (TextView) findViewById(R.id.consoleView);
        joystick = new Joystick(40);

        scrollView = (ScrollView) findViewById(R.id.consoleScrollView);

        prepareConnectionDialog();
    }

    private void prepareConnectionDialog () {
        BluetoothManager.registerDataListener(this);
        BluetoothManager.registerStateListener(this);
        BluetoothManager.sendData(new Instruction(Instruction.Type.MODE_MANUEL).generateCommand());
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_manual, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    float direction [];
    private Instruction instructionToSend;


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        //char c;

        //c = joystick.getDirection((int)event.getX(), (int)event.getY());
        direction = joystick.get360Direction((int)event.getX(), (int)event.getY());

        instructionToSend = new Instruction(Instruction.Type.DEPLACEMENT, Integer.toString((int)(direction[0]*100)), Integer.toString((int)(direction[1]*100)));

        if(event.getAction() == MotionEvent.ACTION_DOWN && !joystickPressed) {
            joystickPressed = true;

            new Thread(new Runnable() {
                @Override
                public void run() {
                    while(joystickPressed) {
                        try {
                            BluetoothManager.sendData(instructionToSend.generateCommand());
                            Thread.sleep(250);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }).start();
        }
        else if(event.getAction() == MotionEvent.ACTION_UP)
            joystickPressed = false;

        return true;
    }


    public void command (View view) {
        Button button = (Button) view;
        String text = button.getText().toString();

        Instruction instruction = null;

        switch(text) {
            case "PinceCylON" :
                instruction = new Instruction(Instruction.Type.CYL_PINCE, 1);
                break;
            case "PinceCylOFF" :
                instruction = new Instruction(Instruction.Type.CYL_PINCE, 0);
                break;
            case "PinceGobON" :
                instruction = new Instruction(Instruction.Type.GOB_PINCE, 1);
                break;
            case "PinceGobOFF" :
                instruction = new Instruction(Instruction.Type.GOB_PINCE, 0);
                break;
            case "BarreGobON" :
                instruction = new Instruction(Instruction.Type.BARRE_POP, 1);
                break;
            case "BarreGobOFF" :
                instruction = new Instruction(Instruction.Type.BARRE_POP, 0);
                break;
            case "BarreClapON" :
                instruction = new Instruction(Instruction.Type.BARRE_CLAPET, 0);
                break;
            case "BarreClapOFF" :
                instruction = new Instruction(Instruction.Type.BARRE_CLAPET, 1);
                break;
            case "Avancer" :
                instruction = new Instruction(Instruction.Type.AVANCER, 300);
                break;
            case "Reculer" :
                instruction = new Instruction(Instruction.Type.AVANCER, -300);
                break;
        }

        if(instruction != null)
            BluetoothManager.sendData(instruction.generateCommand());
    }

    @Override
    public void receiveData(String data) {
        addMessageToConsole("Robot", data);
    }

    @Override
    public void initialized() {
        addMessageToConsole("IA", "Connection initialized! ");
    }

    @Override
    public void connectionEstablished() {
        addMessageToConsole("IA", "Connection established! ");
    }

    @Override
    public void noticeProgress(String info, int percentage) {
        addMessageToConsole("IA", percentage + "% - "+info);
    }

    @Override
    public void connectionLost(String reason) {
        addMessageToConsole("IA", "Connection lost! ");
        BluetoothManager.getBluetoothManager().endConnection();
        //TODO: RECONNECT ?

    }

    private void addMessageToConsole (final String sender, final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                console.append(sender + " ~ " + message + "\n");
                scrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });
    }
}
