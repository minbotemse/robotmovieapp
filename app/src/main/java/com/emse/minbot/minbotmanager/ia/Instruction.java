package com.emse.minbot.minbotmanager.ia;

import java.util.ArrayList;

/**
 * Cette classe représente une instruction qui sera ensuite transmise au robot.
 * Un protocole est mis en place dans la classe Protocol pour changer cela facilement.
 * Created by vincent on 02/05/15.
 */
public class Instruction {

    private String name; // nom associé
    private Type type;
    private ArrayList<String> components = new ArrayList<>();

    public enum Type {
        // COUPE
        DEPLACEMENT,
        CHECKPOINT, ORIENTATION, AVANCER,
        CYL_PINCE,  GOB_PINCE,
        BARRE_CLAPET, BARRE_POP,
        DETECTION_PINCE_CYL, DETECTION_PINCE_GOB,
        DETECTION_AVANT, DETECTION_ARRIERE,
        MODE_AUTOMATIQUE, MODE_MANUEL, ACTION_FINIE,

        // AFTERWORK
        TOURNER, VITESSE, TOUR, STOP,
        PINCE, DETECTION

    }

    public Instruction (Type type, Object... components) {
        this.type = type;
        for(Object component : components)
            this.components.add(""+component);
    }

    public String generateCommand () {
        String command = "";

        command += Protocol.getCommand(type);

        for(String component : components)
            command += Protocol.separator + component;

        command += Protocol.endOfCommand;

        return command;
    }

    @Override
    public String toString () {
        String text = "";

        text += type.toString() + " : ";
        text += generateCommand();

        return text;
    }

    public Type getType() {
        return type;
    }
}
