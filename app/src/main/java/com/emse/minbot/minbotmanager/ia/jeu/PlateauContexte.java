package com.emse.minbot.minbotmanager.ia.jeu;

import com.emse.minbot.minbotmanager.ia.jeu.plateau.Element;
import com.emse.minbot.minbotmanager.ia.jeu.plateau.Robot;

import java.util.ArrayList;

/**
 * Created by victor on 10/05/15.
 */
public class PlateauContexte {
    private ArrayList<Element> elements;
    private Robot robot;
    private Grid grid;
    private double duration; // duree ecouleee pour que le robot effectue ce chemin
    private int score; // score acquis (actions terminees!) au bout du chemin

    public PlateauContexte(Robot robot, ArrayList<Element> elements, Grid grid) {
        this.robot = robot;
        this.elements = elements;
        this.grid = grid;
        duration = 0;
        score = 0;
    }

    public ArrayList<Element> getElements() { return elements; }
    public Robot getRobot() { return robot; }
    public double getDuration() { return duration; }
    public int getScore() { return score; }
    public void setDuration(double duration) { this.duration = duration; }
    public void addScore(int points) { score = score + points; }
    public Grid getGrid () {return grid;}
}
