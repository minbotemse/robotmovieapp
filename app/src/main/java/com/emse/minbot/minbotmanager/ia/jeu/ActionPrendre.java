package com.emse.minbot.minbotmanager.ia.jeu;

import com.emse.minbot.minbotmanager.ia.Instruction;
import com.emse.minbot.minbotmanager.ia.algo.AStar;
import com.emse.minbot.minbotmanager.ia.jeu.plateau.ElementAttrapable;
import com.emse.minbot.minbotmanager.ia.jeu.plateau.Gobelet;
import com.emse.minbot.minbotmanager.ia.jeu.plateau.Pince;

import java.util.ArrayList;

/**
 * Created by victor on 07/05/15.
 */
public class ActionPrendre extends Action {
    private ElementAttrapable element;
    private Pince pince;
    private ArrayList<CheckPoint> robotPath;
    private PlateauContexte contexte;

    public ActionPrendre(Objectif objectif, ElementAttrapable element) {
        super(objectif);
        this.element = element;
    }

    @Override
    public void processContext(PlateauContexte contexte) {
        this.contexte = contexte;
        pince = getPinceCorrespondante(contexte.getRobot().getPinces());

        if(pince == null)
            return;

        // On va à présent générer les différents instructions pour cette action
        // On commence par récupérer quelques informations sur le contexte
        CheckPoint checkPointRobot = contexte.getGrid().getClosest(contexte.getRobot().getCoordonnees());
        CheckPoint checkPointElement = contexte.getGrid().getClosest(element.getCoordonnees());

        // On génère ensuite le chemin pour aller du robot à l'élément
        robotPath = AStar.FindPath(checkPointRobot, checkPointElement);

        // Si l'IA ne trouve pas de chemin, on passe
        if(robotPath == null)
            return;

        instructions.clear();

        instructions.add(new Instruction(Instruction.Type.DETECTION_AVANT, 1));

        // On ajoute tous les checkpoints intermédiaires
        for(int i = 0; i < robotPath.size() - 1; i++)
            instructions.add(robotPath.get(i).createInstruction(0));

        // ainsi que le dernier en checkpoint final
        instructions.add(robotPath.get(robotPath.size()-1).createInstruction(1));

        instructions.add(new Instruction(Instruction.Type.DETECTION_AVANT, 0));

        // Une fois près du cylindre, on s'oriente vers lui
        instructions.add(new Instruction(Instruction.Type.ORIENTATION, pince.getFaceRobot(), element.getCoordonnees().x, element.getCoordonnees().y));

        int sens = pince.getType() == Instruction.Type.CYL_PINCE ? 1 : -1;

        // On ouvre les pinces
        instructions.add(new Instruction(pince.getType(), 0));

        instructions.add(new Instruction(Instruction.Type.DETECTION_AVANT, 1));

        // Ensuite on s'avance vers l'élément
        float dist = (float)Math.sqrt(checkPointElement.getPos().dist2(element.getCoordonnees()));
        instructions.add(new Instruction(Instruction.Type.AVANCER, sens * dist));

        instructions.add(new Instruction(Instruction.Type.DETECTION_AVANT, 0));

        // Puis on referme la pince
        instructions.add(new Instruction(pince.getType(), 1));

        // Et enfin, on demande à l'arduino de vérifier qu'il y a bien un objet dans sa pince
        if(pince.getType() == Instruction.Type.CYL_PINCE)
            instructions.add(new Instruction(Instruction.Type.DETECTION_PINCE_CYL));
        else
            instructions.add(new Instruction(Instruction.Type.DETECTION_PINCE_GOB));

        instructions.add(new Instruction(Instruction.Type.ACTION_FINIE));
    }

    @Override
    public float calculateCost() {
        //return objective.getPointsGagnes() / element.getCoordonnees().dist2(contexte.getRobot().getCoordonnees());

        // On va approx le coût avec le cout de la distance
        return CheckPoint.totalPathLength(robotPath);
    }

    @Override
    public void postAction() {
        // C'est approximatif, mais ça devrait aller
        contexte.getRobot().setCoordonnees(robotPath.get(robotPath.size()-1).getPos());
        pince.setObject(element);
    }

    @Override
    public boolean canBeDone() {
        return (pince != null && robotPath != null);
    }

    private Pince getPinceCorrespondante(Pince[] pinces) {
        Pince returnPince = null;
        // Si l'élément est un gobelet, seule la pince gobelet peut l'attraper
        if(element instanceof Gobelet) {
            for(Pince pince : pinces)
                if(pince.getType() == Instruction.Type.GOB_PINCE && pince.isEmpty())
                    return pince;
        }
        else {
            // Si l'élément est un cylindre, n'importe quelle pince suffira,
            for(Pince pince : pinces)
                if(pince.isEmpty()) {
                    if(returnPince == null)
                        returnPince = pince;
                    else
                        if (pince.getType() == Instruction.Type.CYL_PINCE)
                            return pince;
                        // cependant on préfèrera quand même une pince de type cylindre
                        // donc si la pince a retourner est actuellement de type gobelet on peut la changer
                        if(returnPince.getType() == Instruction.Type.GOB_PINCE)
                            returnPince = pince;
                }
        }

        return returnPince;
    }

}
