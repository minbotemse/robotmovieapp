package com.emse.minbot.minbotmanager;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.io.PrintWriter;
import java.io.StringWriter;

import at.markushi.ui.CircleButton;

/**
 * Created by Maxence on 08/04/2015.
 */
public class MenuActivity extends Activity{

    CircleButton manualbutton = null;
    CircleButton autoButton = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);


        manualbutton = (CircleButton)findViewById(R.id.manualButton);
        manualbutton.setOnClickListener(manualListener);

        autoButton = (CircleButton)findViewById(R.id.autoButton);
        autoButton.setOnClickListener(autoListener);

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());
    }

    private View.OnClickListener autoListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            new AlertDialog.Builder(MenuActivity.this)
                    .setPositiveButton("Agressif", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.dismiss();

                            Intent intent = new Intent(MenuActivity.this, AutoActivity.class);
                            intent.putExtra("IAType", "AGRESSIF");
                            startActivity(intent);
                        }
                    })
                    .setNeutralButton("Peureux", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.dismiss();

                            Intent intent = new Intent(MenuActivity.this, AutoActivity.class);
                            intent.putExtra("IAType", "PEUREUX");
                            startActivity(intent);
                        }
                    })
                    .show();
        }
    };

    private View.OnClickListener manualListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MenuActivity.this, ManualActivity.class);
            startActivity(intent);

        }
    };

    public class ExceptionHandler implements java.lang.Thread.UncaughtExceptionHandler {
        private final String LINE_SEPARATOR = "\n";
        public  final String LOG_TAG = ExceptionHandler.class.getSimpleName();

        @SuppressWarnings("deprecation")
        public void uncaughtException(Thread thread, Throwable exception) {
            StringWriter stackTrace = new StringWriter();
            exception.printStackTrace(new PrintWriter(stackTrace));

            StringBuilder errorReport = new StringBuilder();
            errorReport.append(stackTrace.toString());

            Log.e(LOG_TAG, "AZERTYUIOP" + errorReport.toString());

            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(10);
        }
    }
}
