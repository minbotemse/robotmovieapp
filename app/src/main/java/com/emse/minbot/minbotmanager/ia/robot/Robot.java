package com.emse.minbot.minbotmanager.ia.robot;

import com.emse.minbot.minbotmanager.ia.Instruction;
import com.emse.minbot.minbotmanager.ia.jeu.Afterwork2015;
import com.emse.minbot.minbotmanager.ia.utils.MoodListener;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by vincent on 09/06/15.
 */
public class Robot {

    private Afterwork2015.IAType iaType;
    private Random random;
    private MoodListener.Mood mood = MoodListener.Mood.OK;
    private float PI = (float) Math.PI;
    private float vitesse;
    private static final float MIN_VITESSE = 50;
    private static final float MAX_VITESSE = 300;

    // Means if he detected something recently
    private boolean inActionMode = false;
    private boolean firstTimeInActionMode = false;
    private long timeOfDetection = 0;
    private static final int ACTION_TIME = 15000000;
    private float collisionAngle;

    public Robot(Afterwork2015.IAType iaType) {
        this.iaType = iaType;
        this.random = new Random();
        this.vitesse = 50;
    }

    public ArrayList<Instruction> determineWhatToDo () {
        ArrayList<Instruction> instructions = new ArrayList<>();

        // S'il était en mode action et que suffisamment de temps s'est écoulé,
        // on retourne au mode normal
        if(inActionMode && System.nanoTime() - timeOfDetection > ACTION_TIME) {
            inActionMode = false;
            vitesse = 50;
            mood = MoodListener.Mood.OK;
        }

        // Si tout va bien, le robot se promène
        if(!inActionMode) {
            // Une chance sur quatre
            if(random.nextBoolean() && random.nextBoolean()) {
                // Le robot va tourner
                float angleDiff = random.nextFloat()*(2*PI/3.0f)/Float.MAX_VALUE - PI/3.0f;
                instructions.add(new Instruction(Instruction.Type.TOURNER, angleDiff));
            }

            if(random.nextBoolean() && random.nextBoolean()) {

            }
        }
        // Sinon, on est toujours en ActionMode (énervé ou effrayé)
        else {
            if(firstTimeInActionMode) {
                //TODO: do things with collisionAngle

                firstTimeInActionMode = false;
            }
        }


        return instructions;
    }


    public void somethingDetected (float angle) {
        timeOfDetection = System.nanoTime();
        inActionMode = true;
        firstTimeInActionMode = true;
        collisionAngle = angle;
    }

    public MoodListener.Mood getMood () {
        return mood;
    }
}
