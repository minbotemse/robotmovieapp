package com.emse.minbot.minbotmanager.ia.jeu;

import com.emse.minbot.minbotmanager.ia.Instruction;
import com.emse.minbot.minbotmanager.ia.algo.AStar;
import com.emse.minbot.minbotmanager.ia.jeu.plateau.BarreRotative;
import com.emse.minbot.minbotmanager.ia.jeu.plateau.Distributeur;
import com.emse.minbot.minbotmanager.ia.jeu.plateau.Element;
import com.emse.minbot.minbotmanager.ia.jeu.plateau.Gobelet;
import com.emse.minbot.minbotmanager.ia.jeu.plateau.Pince;

import java.util.ArrayList;

/**
 * Created by victor on 07/05/15.
 */
public class ActionBallesDuDistributeur extends Action {
    protected final static double dureePrevue = 0;
    private Gobelet gobelet;
    private Distributeur distributeur;
    private PlateauContexte contexte;
    private Pince pince;
    private ArrayList<CheckPoint> robotPath;

    public ActionBallesDuDistributeur(Objectif objectif, Gobelet gobelet) {
        super(objectif);
        this.gobelet = gobelet;
    }

    private Pince getPinceCorrespondante(Pince[] pinces) {
        for (Pince p: pinces) {
            if (p.getObjet() == gobelet)
                return p;
        }
        return null;
    }


    @Override
    public void processContext(PlateauContexte contexte) {
        this.contexte = contexte;
        CheckPoint checkPointRobot = contexte.getGrid().getClosest(contexte.getRobot().getCoordonnees()), checkPointElement = null;
        ArrayList<CheckPoint> newPath = null;

        robotPath = null;

        // Trouver le distributeur correspondant
        float distance = Float.MAX_VALUE, valeur;
        for (Element element: this.contexte.getElements())
        {
            if (element.getClass() == Distributeur.class) {
                checkPointElement = contexte.getGrid().getClosest(element.getCoordonnees());
                newPath = AStar.FindPath(checkPointRobot, checkPointElement);
                if (newPath == null)
                    continue;
                valeur = CheckPoint.totalPathLength(newPath);
                if (valeur < distance) {
                    distributeur = (Distributeur) element;
                    distance = valeur;
                    robotPath = newPath;
                }
            }
        }

        if (robotPath == null)
            return;

        // Trouver la pince correspondante
        pince = getPinceCorrespondante(this.contexte.getRobot().getPinces());

        instructions.clear();

        instructions.add(new Instruction(Instruction.Type.DETECTION_AVANT, 1));

        // On ajoute tous les checkpoints intermédiaires
        for(int i = 0; i < robotPath.size() - 1; i++)
            instructions.add(robotPath.get(i).createInstruction(0));

        // ainsi que le dernier en checkpoint final
        instructions.add(robotPath.get(robotPath.size()-1).createInstruction(1));

        instructions.add(new Instruction(Instruction.Type.DETECTION_AVANT, 0));

        // On oriente le robot vers le distributeur
        instructions.add(new Instruction(Instruction.Type.ORIENTATION, 0, distributeur.getCoordonnees().x, distributeur.getCoordonnees().y));

        instructions.add(new Instruction(Instruction.Type.DETECTION_AVANT, 1));

        // On avance le robot
        float dist = (float)Math.sqrt(checkPointElement.getPos().dist2(distributeur.getCoordonnees()));
        // mais pas trop...
        dist *= 0.80f;
        instructions.add(new Instruction(Instruction.Type.AVANCER, dist));

        instructions.add(new Instruction(Instruction.Type.DETECTION_AVANT, 0));

        // On oriente la face du robot avec la barre rotative vers le clapet
        BarreRotative barre = null;
        for (BarreRotative barreTempo : contexte.getRobot().getBarres())
            if(barreTempo.getType() == Instruction.Type.BARRE_POP)
                barre = barreTempo;

        instructions.add(new Instruction(Instruction.Type.ORIENTATION, barre.getFaceRobot(), distributeur.getCoordonnees().x, distributeur.getCoordonnees().y));

        // On active ensuite la barre plusieurs fois pour faire tomber plusieurs balles
        for(int i = 0; i < 2; i++) {
            // On active la barre
            instructions.add(new Instruction(Instruction.Type.BARRE_POP, 1));

            // On désactive la barre
            instructions.add(new Instruction(Instruction.Type.BARRE_POP, 0));
        }

        instructions.add(new Instruction(Instruction.Type.ACTION_FINIE));
    }

    @Override
    public float calculateCost() {
        float bonus = -2000;

        // On va approx le coût avec le cout de la distance
        return CheckPoint.totalPathLength(robotPath) + bonus;
    }

    @Override
    public void postAction() {
        contexte.getRobot().setCoordonnees(distributeur.getCoordonnees());
        //TODO: Gérer le nombre de balles restant par le distributeur
    }

    public boolean canBeDone() {
        return (robotPath != null && distributeur != null && pince != null);
    }


}
